const express = require("express");
const redis = require("redis");

const app = express();
const client = redis.createClient({ host: "redis" });

app.use(express.json());

app.get("/messages", (req, res) => {
  client.lRange("messages", 0, -1).then(reply => {
    res.send(reply);
  }).catch(err => res.send(err.message));
});

app.post("/messages", (req, res) => {
  const { message } = req.body;
  client.rPush("messages", message).then(reply => {
    res.send({ message: "Message added!" });
  }).catch(err => res.send(err.message));
});

app.listen(3000, () => {
  console.log("App is running on port 3000");
});