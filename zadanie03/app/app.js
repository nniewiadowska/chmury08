const express = require("express");
const redis = require("redis");
const { Pool } = require("pg");

const app = express();
app.use(express.json());

const redisClient = redis.createClient({ host: "redis" });
const pool = new Pool({ host: "db", user: "postgres", password: "password", database: "postgres" });

app.post("/messages", (req, res) => {
  const { message } = req.body;
  redisClient.rPush("messages", message).then(reply => {
    res.status(201).send({ message: "Message added!" });
  }).catch(err => {
    console.error(err.message);
  });
});

app.get("/messages", (req, res) => {
  redisClient.lRange("messages", 0, -1).then(reply => {
    res.send(reply);
  }).catch(err => {
    console.error(err.message);
  });
});

app.post("/users", async (req, res) => {
  const { name } = req.body;
  const result = await pool.query("INSERT INTO users (name) VALUES ($1) RETURNING id", [name]);
  res.status(201).send({ id: result.rows[0].id });
});

app.get("/users", async (req, res) => {
  const result = await pool.query("SELECT * FROM users");
  res.send(result.rows);
});

app.listen(3000, () => console.log("App is listening on port 3000"));